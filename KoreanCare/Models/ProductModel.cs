﻿namespace koreanCare.Models
{
    public class ProductModel
    {
        public int ProductId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Company Company { get; set; }
        public ProductType ProductType { get; set; }
    }

    public enum Company
    {

    }

    public enum ProductType
    {

    }
}
