﻿using System.Threading.Tasks;
using koreanCare.Models;
using koreanCare.Services;
using Microsoft.AspNetCore.Mvc;

namespace koreanCare.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> ProductList()
        {
            return View(await _productService.Get(Limit, Offset));
        }

        [HttpGet]
        public async Task<IActionResult> SingleProduct(int productId)
        {
            return View(await _productService.Get(productId));
        }

        [HttpGet]
        public IActionResult NewProduct() => View();

        [HttpPost]
        public async Task<IActionResult> NewProduct(ProductModel product)
        {
            await _productService.Create(product);

            return RedirectToAction("ProductList", "Products");
        }
    }
}
