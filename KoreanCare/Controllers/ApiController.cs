﻿using System;
using koreanCare.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace koreanCare.Controllers
{
    public abstract class ApiController : Controller
    {
        private const int DefaultLimit = 100;
        private const int DefaultOffset = 0;

        private readonly Lazy<int> limit;
        private readonly Lazy<int> offset;

        protected ApiController()
        {
            limit = new Lazy<int>(() => Request.GetIntValueFromQueryString(nameof(limit)) ?? DefaultLimit);
            offset = new Lazy<int>(() => Request.GetIntValueFromQueryString(nameof(offset)) ?? DefaultOffset);
        }

        protected int Limit => limit.Value;
        protected int Offset => offset.Value;
    }
}
