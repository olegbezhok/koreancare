﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Infrastructure;
using DataAccess.Queries.Products;
using DataAccess.Repositories;
using Domain.Queries.Products;
using Domain.Repositories;
using koreanCare.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NHibernate;
using Service.Infrastructure;
using Service.Interfaces;
using Service.Services;

namespace koreanCare.Infrastructure.DI
{
    public static class Registration
    {
        public static void RegisterServiceDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .RegisterApiApiService(configuration)
                .RegisterCoreService(configuration);
        }

        private static IServiceCollection RegisterApiApiService(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IProductService, ProductService>();

            return services;
        }

        private static IServiceCollection RegisterCoreService(this IServiceCollection services,
            IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            services.AddSingleton<ISessionFactoryProvider>(x => new SessionFactoryProvider(connectionString));
            services.AddScoped(x => new NHibernateUnitOfWork(x.GetRequiredService<ISessionFactoryProvider>()));
            services.AddTransient<IUnitOfWork>(s => s.GetRequiredService<NHibernateUnitOfWork>());
            services.AddTransient<IDbContext>(s => s.GetRequiredService<NHibernateUnitOfWork>());


            services.AddTransient<IProductAppService, ProductAppService>();
            services.AddTransient<IProductsQuery, ProductsQuery>();
            services.AddTransient<IProductRepository, ProductRepository>();

            return services;
        }
    }
}
