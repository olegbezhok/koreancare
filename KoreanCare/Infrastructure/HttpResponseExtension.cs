﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace koreanCare.Infrastructure
{
    internal static class HttpResponseExtension
    {
        public static HttpResponse AddTotalCountHeader(this HttpResponse response, int totalCount)
        {
            response.Headers.Add(CustomExposeHeader.XResourceTotal, totalCount.ToString());

            return response;
        }

        public static HttpResponse AddLinkHeader(this HttpResponse response, IUrlHelper urlHelper, int limit, int offset, int totalCount)
        {

            string currentRouteName = urlHelper.ActionContext.RouteData.DataTokens.TryGetValue("RouteName", out var value) ? value.ToString() : null,
                   scheme = response.HttpContext.Request.Scheme,
                   host = response.HttpContext.Request.Host.ToString();

            var parameters = response.HttpContext.Request.GetQueryStringParams(k => !String.Equals(k, nameof(offset), StringComparison.InvariantCultureIgnoreCase) &&
                                                                                    !String.Equals(k, nameof(limit), StringComparison.InvariantCultureIgnoreCase));

            foreach (var routeKeyValue in urlHelper.ActionContext.ActionDescriptor.RouteValues)
            {
                if (!parameters.ContainsKey(routeKeyValue.Key))
                {
                    parameters.Add(routeKeyValue);
                }

            }

            var maxOffset = totalCount > 0 && limit > 0
                ? ((int)Math.Ceiling(totalCount / (double)limit) - 1) * limit
                : 0;

            var links = new List<string> { $"{urlHelper.RouteUrl(currentRouteName, GetParams(parameters, limit, 0), scheme, host)}; rel=\"first\"" };

            if (offset > 0)
            {
                links.Add($"{urlHelper.RouteUrl(currentRouteName, GetParams(parameters, limit, offset - limit > 0 ? (offset - limit) : 0), scheme, host)}; rel=\"previous\"");
            }
            if (offset < maxOffset)
            {
                links.Add($"{urlHelper.RouteUrl(currentRouteName, GetParams(parameters, limit, offset + limit), scheme, host)}; rel=\"next\"");
            }

            links.Add($"{urlHelper.RouteUrl(currentRouteName, GetParams(parameters, limit, maxOffset), scheme, host)}; rel=\"last\"");

            response.Headers.Add(CustomExposeHeader.Link, String.Join(", ", links));

            return response;
        }

        private static IDictionary<string, string> GetParams(IDictionary<string, string> source, int limit, int offset)
        {
            var parameters = new Dictionary<string, string>(source);
            parameters.Add(nameof(limit), limit.ToString());
            parameters.Add(nameof(offset), offset.ToString());

            return parameters;
        }
    }
}
