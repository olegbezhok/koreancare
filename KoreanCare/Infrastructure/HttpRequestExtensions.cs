﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace koreanCare.Infrastructure
{
    internal static class HttpRequestExtensions
    {
        public static int? GetIntValueFromQueryString(this HttpRequest request, string valueName)
        {
            if (request.Query.TryGetValue(valueName, out var value))
            {
                return int.TryParse(value, out int result) ? result : (int?) null;
            }

            return null;
        }

        public static IDictionary<string, string> GetQueryStringParams(this HttpRequest request,
            Func<string, bool> whereClause)
        {
            return request.Query.Keys.Where(whereClause)
                .Select(k => new KeyValuePair<string, string>(k, request.Query[k]))
                .ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
