﻿namespace koreanCare.Infrastructure
{
    public static class CustomExposeHeader
    {
        public const string XResourceTotal = "X-Resource-Total";
        public const string Link = "Link";
    }
}
