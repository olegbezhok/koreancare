﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using koreanCare.Mappings;
using koreanCare.Models;
using Service.Interfaces;

namespace koreanCare.Services
{
    public interface IProductService
    {
        Task<(IEnumerable<ProductModel> Products, int TotalCount)> Get(int limit, int offset);
        Task Create(ProductModel product);
        Task<ProductModel> Get(int productId);
    }

    public class ProductService : IProductService
    {
        private readonly IProductAppService _productAppService;

        public ProductService(IProductAppService productAppService)
        {
            _productAppService = productAppService;
        }

        public async Task<(IEnumerable<ProductModel> Products, int TotalCount)> Get(int limit, int offset)
        {
            var result = await _productAppService.Get(limit, offset);

            return (result.Products.Select(p => p.ToResource()), result.TotalCount);
        }

        public async Task Create(ProductModel product)
        {
            await _productAppService.Create(product.ToDto());
        }

        public async Task<ProductModel> Get(int productId)
        {
            var result = await _productAppService.Get(productId);

            return result.ToResource();
        }
    }
}
