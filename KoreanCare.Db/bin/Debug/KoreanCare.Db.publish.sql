﻿/*
Скрипт развертывания для KoreanCare

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "KoreanCare"
:setvar DefaultFilePrefix "KoreanCare"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL,
                RECOVERY FULL 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET PAGE_VERIFY NONE 
            WITH ROLLBACK IMMEDIATE;
    END


GO

GO
PRINT N'Выполняется создание [dbo].[Basket]...';


GO
CREATE TABLE [dbo].[Basket] (
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [ProductId] INT              NOT NULL,
    [Active]    BIT              NOT NULL,
    [OrderDate] DATETIME         NULL,
    CONSTRAINT [PK_Basket] PRIMARY KEY CLUSTERED ([UserId] ASC, [ProductId] ASC, [Active] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[Company]...';


GO
CREATE TABLE [dbo].[Company] (
    [CompanyId]   INT            NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([CompanyId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[Product]...';


GO
CREATE TABLE [dbo].[Product] (
    [ProductId]     INT             NOT NULL,
    [Image]         VARBINARY (MAX) NULL,
    [ProductTypeId] INT             NOT NULL,
    [CompanyId]     INT             NOT NULL,
    [Price]         INT             NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[ProductType]...';


GO
CREATE TABLE [dbo].[ProductType] (
    [ProductTypeId] INT            NOT NULL,
    [Description]   NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED ([ProductTypeId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[Role]...';


GO
CREATE TABLE [dbo].[Role] (
    [RoleId]      INT           NOT NULL,
    [Description] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[User]...';


GO
CREATE TABLE [dbo].[User] (
    [UserId]      UNIQUEIDENTIFIER NOT NULL,
    [UserName]    NVARCHAR (250)   NOT NULL,
    [FirstName]   NVARCHAR (50)    NOT NULL,
    [LastName]    NVARCHAR (50)    NOT NULL,
    [Password]    NVARCHAR (50)    NOT NULL,
    [RoleId]      INT              NOT NULL,
    [PhoneNumber] INT              NULL,
    [Address]     NVARCHAR (200)   NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

ALTER TABLE dbo.[User]
	ADD CONSTRAINT FK_User_Role
	FOREIGN KEY (RoleId)
	REFERENCES dbo.[Role] (RoleId)
GO

GO
/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
GO

GO
PRINT N'Обновление завершено.';


GO
