﻿CREATE TABLE [dbo].[Basket]
(
	UserId UNIQUEIDENTIFIER NOT NULL,
	ProductId INT,
	Active BIT,
	OrderDate DATETIME

	CONSTRAINT PK_Basket PRIMARY KEY (UserId, ProductId, Active)
)
