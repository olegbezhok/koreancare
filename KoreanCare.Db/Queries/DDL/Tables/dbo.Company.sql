﻿CREATE TABLE [dbo].[Company]
(
	CompanyId INT IDENTITY(1,1) NOT NULL,
	Description NVARCHAR(100) NOT NULL

	CONSTRAINT PK_Company PRIMARY KEY (CompanyId)
)
