﻿CREATE TABLE [dbo].[ProductType]
(
    ProductTypeId INT IDENTITY(1,1)  NOT NULL,
	Description NVARCHAR (200) NOT NULL
	
	CONSTRAINT PK_ProductType PRIMARY KEY (ProductTypeId)
)
