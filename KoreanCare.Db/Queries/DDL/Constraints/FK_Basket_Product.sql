﻿ALTER TABLE dbo.Basket
	ADD CONSTRAINT FK_Basket_Product
	FOREIGN KEY (ProductId)
	REFERENCES Product (ProductId)