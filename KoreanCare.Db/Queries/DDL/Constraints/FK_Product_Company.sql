﻿ALTER TABLE dbo.Product
	ADD CONSTRAINT FK_Product_Company
	FOREIGN KEY (CompanyId)
	REFERENCES Company (CompanyId)