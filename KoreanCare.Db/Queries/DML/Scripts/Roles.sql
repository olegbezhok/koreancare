﻿INSERT INTO [dbo].[Role] ([RoleId], [Description])
VALUES
    (1, N'Admin'),
    (2, N'Editor'),
    (3, N'User');