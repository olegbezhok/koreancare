﻿INSERT INTO [dbo].[Product] ([ProductTypeId],[CompanyId],[Price],[Name],[Description])
VALUES
    (1,1,15,N'ANSKIN Ultimate',
    N'Южнокорейская компания предлагает для вас прекрасную альтернативу — активатор для размешивания маски Ultimate Solution Modeling Activator от ANSKIN. Пьете ли вы воду из-под крана? '),   
    (5,2,25.55,N'BAVIPHAT Urban Dollkiss Tongkeun Snail Cream',
    N'Крем для лица и шеи с муцином улитки разработан для борьбы с возрастными изменениями, он разглаживает морщины, препятствует появлению новых морщин, повышает упругость, оздоровляет кожу.'),
    (4,2,30,N'Тинт для губ BAVIPHAT Magic Lip Tint',
    N'Тинт для губ от BAVIPHAT Magic Lip Tint не только дарит губам нежные и естественные цвета, но и ухаживает за кожей губ.'),
    (3,3,20,N'G9 3D Volume Gum Mask',
    N'Омолаживающая маска 3D Volume Gum от G9 — это прекрасное средство для борьбы с возрастными изменениями,а также эффективно осветляет пигментацию.'),
    (2,3,30,N'G9 Grapefruit Vita Peeling Gel',
    N'Пилинг-гель для лица BERRISOM G9 Skin Grapefruit Vita Peeling Gel предназначен для еженедельного ухода за кожей любого типа.'),
    (6,4,20,N'DEOPROCE Healing Mix & Plus Body Cleanser Berry',
    N'Гель для душа DEOPROCE Healing Mix & Plus Body Cleanser Berry с экстрактами клубники, черники и граната качественно очищает кожу и наполняет ее жизненной силой. Пышная пена удаляет все загрязнения, излишки жира и отмершие клетки, увлажняет и ускоряет регенерацию клеток.');
   
    