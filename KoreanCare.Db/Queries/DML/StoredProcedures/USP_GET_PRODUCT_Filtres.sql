﻿CREATE PROCEDURE [dbo].[USP_GET_PRODUCT_Filtres]
(
@productName nvarchar(50),
@companyId int,
@productTypeId int
)			
AS
BEGIN

SELECT Product .ProductId , Product .Name , Product .ProductTypeId , Product .Price , C.Description
FROM Product 
JOIN Company C ON Product.CompanyId = C.CompanyId
JOIN ProductType PT ON PT.ProductTypeId = Product.ProductTypeId
WHERE Product.Name LIKE @productName
and(@companyId IS NULL OR C.CompanyId = @companyId)
and (@productTypeId IS NULL OR Product.ProductTypeId = @productTypeId)
END