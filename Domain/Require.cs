﻿using System;

namespace Domain
{
    public static class Require
    {
        public static void That(bool expectedCondition, string errorMessage)
        {
            if (!expectedCondition)
                throw new DomainRuleViolationException(errorMessage);
        }

        public static void NotEmptyOrWhiteSpace(string value, string message)
        {
            That(!string.IsNullOrWhiteSpace(value), message);
        }

        public static void ArgNotNull(object value, string property)
        {
            if (value == null)
                throw new ArgumentNullException(property);
        }
    }
}
