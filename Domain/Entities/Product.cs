﻿using Domain.Entities.ValueTypes;

namespace Domain.Entities
{
    public class Product : IAggregateRoot
    {
        public virtual int ProductId { get; protected set; }
        public virtual decimal Price { get; protected set; }
        public virtual string Name { get; protected set; }
        public virtual Company Company { get; protected set; }
        public virtual ProductType ProductType { get; protected set; }
        public virtual string Description { get; protected set; }

        public Product(string name, decimal price, Company company, ProductType productType,
            string description)
        {
            Require.ArgNotNull(name, nameof(name));
            Require.ArgNotNull(price, nameof(price));
            Require.ArgNotNull(company, nameof(company));
            Require.ArgNotNull(productType, nameof(productType));
            Require.ArgNotNull(description, nameof(description));

            Name = name;
            Price = price;
            Company = company;
            ProductType = productType;
            Description = description;
        }
    }
}
