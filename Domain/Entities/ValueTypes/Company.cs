﻿namespace Domain.Entities.ValueTypes
{
    public class Company : Enumeration
    {
        private Company(string name) : base(name) { }
    }
}
