﻿namespace Domain.Entities.ValueTypes
{
    public class ProductType : Enumeration
    {
        public ProductType(string name) : base(name) { }
    }
}
