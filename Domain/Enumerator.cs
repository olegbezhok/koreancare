﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Domain
{
    [ExcludeFromCodeCoverage]
    public abstract class Enumeration : IComparable
    {
        public string Name { get; }

        protected Enumeration(string name)
        {
            Name = name;
        }

        public override string ToString() => Name;

        public static IEnumerable<T> GetAll<T>() where T : Enumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration;

            if (!(obj is Enumeration))
                return false;

            var typeMatches = GetType() == obj.GetType();
            var valueMatches = Name == otherValue.Name;

            return typeMatches && valueMatches;
        }

        public override int GetHashCode() => Name.GetHashCode();

        public int CompareTo(object obj) => Name?.CompareTo(((Enumeration)obj).Name) ?? 1;
    }
}
