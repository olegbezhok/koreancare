﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities.ValueTypes;

namespace Domain.Queries.Products
{
    public interface IProductsQuery
    {
        Task<(IEnumerable<ProductReadModel> Products, int TotalCount)> Execute(int limit, int offset, int? productId = null);
    }

    public class ProductReadModel
    {
        public int ProductId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Company Company { get; set; }
        public ProductType ProductType { get; set; }
    }

    public enum OrderBy
    {
        Name,
        Price
    }

    public enum Order
    {
        Asc,
        Desc
    }
}
