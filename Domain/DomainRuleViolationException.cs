﻿using System;
using System.Runtime.Serialization;

namespace Domain
{
    public class DomainRuleViolationException : Exception
    {
        protected DomainRuleViolationException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }

        public DomainRuleViolationException(string message)
            : base(message) { }
        public DomainRuleViolationException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
