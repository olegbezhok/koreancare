﻿using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IProductRepository
    {
        Task<Product> Get(int productId);
        Task Save(Product product);
    }
}
