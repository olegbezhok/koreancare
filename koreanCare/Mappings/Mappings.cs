﻿using koreanCare.Infrastructure;
using koreanCare.Models;
using Service.Infrastructure;
using Service.Interfaces;
using Service.Mappings;
using Company = Service.Interfaces.Company;
using ProductType = Service.Interfaces.ProductType;

namespace koreanCare.Mappings
{
    public static class Mappings
    {
        #region Product Map

        public static ProductModel ToResource(this ProductDto dto)
        {
            return new ProductModel
            {
                ProductId = dto.ProductId,
                Description = dto.Description,
                Name = dto.Name,
                Company = CompanyMap[dto.Company],
                Price =  dto.Price,
                ProductType = ProductTypeMap[dto.ProductType]
            };
        }

        public static ProductDto ToDto(this ProductModel model)
        {
            return new ProductDto
            {
                ProductId = model.ProductId,
                Description = model.Description,
                Name = model.Name,
                Company = CompanyMap[model.Company],
                Price = model.Price,
                ProductType = ProductTypeMap[model.ProductType]
            };
        }

        #endregion

        #region Company Map
        
        private static readonly BidirectionalDictionary<Models.Company, Company> CompanyMap =
            new BidirectionalDictionary<Models.Company, Company>
                { };

        #endregion

        #region ProductType Map
        
        private static readonly BidirectionalDictionary<Models.ProductType, ProductType> ProductTypeMap =
            new BidirectionalDictionary<Models.ProductType, ProductType>
                { };

        #endregion
    }
}
