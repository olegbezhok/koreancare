﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Queries.Products;

namespace Service.Interfaces
{
    public interface IProductAppService
    {
        Task<(IEnumerable<ProductDto> Products, int TotalCount)> Get(int limit, int offset);
        Task Create(ProductDto product);
        Task<ProductDto> Get(int productId);
    }

    public class ProductDto
    {
        public int ProductId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Company Company { get; set; }
        public ProductType ProductType { get; set; }
    }

    public enum Company
    {

    }

    public enum ProductType
    {

    }
}
