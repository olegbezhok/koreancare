﻿using Domain.Queries.Products;
using Service.Infrastructure;
using Service.Interfaces;

namespace Service.Mappings
{
    public static class Mappings
    {
        #region Product Map

        public static ProductDto ToDto(this ProductReadModel domain)
        {
            return new ProductDto
            {
                ProductId = domain.ProductId,
                Name = domain.Name,
                Description = domain.Description
            };
        }

        #endregion

        #region Company Map

        public static Domain.Entities.ValueTypes.Company ToDomain(this Company type) => CompanyMap[type];

        public static Company ToDto(this Domain.Entities.ValueTypes.Company type) => CompanyMap[type];

        private static readonly BidirectionalDictionary<Company, Domain.Entities.ValueTypes.Company> CompanyMap =
            new BidirectionalDictionary<Company, Domain.Entities.ValueTypes.Company>
            { };

        #endregion

        #region ProductType Map

        public static Domain.Entities.ValueTypes.ProductType ToDomain(this ProductType type) => ProductTypeMap[type];

        public static ProductType ToDto(this Domain.Entities.ValueTypes.ProductType type) => ProductTypeMap[type];

        private static readonly BidirectionalDictionary<ProductType, Domain.Entities.ValueTypes.ProductType> ProductTypeMap =
            new BidirectionalDictionary<ProductType, Domain.Entities.ValueTypes.ProductType>
            { };

        #endregion
    }
}
