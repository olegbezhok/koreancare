﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Queries.Products;
using Domain.Repositories;
using Service.Infrastructure;
using Service.Interfaces;
using Service.Mappings;

namespace Service.Services
{
    public class ProductAppService : IProductAppService
    {
        private readonly IProductsQuery _productsQuery;
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductAppService(
            IProductsQuery productsQuery,
            IProductRepository productRepository,
            IUnitOfWork unitOfWork)
        {
            _productsQuery = productsQuery;
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
        }

        public async Task<(IEnumerable<ProductDto> Products, int TotalCount)> Get(int limit, int offset)
        {
            using (_unitOfWork.BeginTransaction())
            {
                var result = await _productsQuery.Execute(limit, offset);

                return (result.Products.Select(x => x.ToDto()), result.TotalCount);
            }
        }

        public async Task Create(ProductDto productDto)
        {
            using (var transaction = _unitOfWork.BeginTransaction())
            {
                await Try.Action(async () =>
                {
                    var product = new Product(productDto.Name, productDto.Price,
                        productDto.Company.ToDomain(), productDto.ProductType.ToDomain(), productDto.Description
                    );

                    await _productRepository.Save(product);

                    await transaction.Commit();

                    return product.ProductId;
                });
            }
        }

        public async Task<ProductDto> Get(int productId)
        {
            using (_unitOfWork.BeginTransaction())
            {
                var result = await _productsQuery.Execute(1, 0, productId);

                return result.Products.FirstOrDefault().ToDto();
            }
        }
    }
}
