﻿using System;
using System.Threading.Tasks;
using Domain;

namespace Service.Infrastructure
{
    public static class Try
    {
        public static void Action(Action a)
        {
            Require.ArgNotNull(a, "action");
            try
            {
                a();
            }
            catch (DomainRuleViolationException e)
            {
                throw new InvalidApplicationServiceCallException(e.Message, e);
            }
        }

        public static T Action<T>(Func<T> a)
        {
            Require.ArgNotNull(a, "action");
            try
            {
                return a();
            }
            catch (DomainRuleViolationException e)
            {
                throw new InvalidApplicationServiceCallException(e.Message, e);
            }
        }

        public static async Task Action(Func<Task> a)
        {
            Require.ArgNotNull(a, "action");
            try
            {
                await a();
            }
            catch (DomainRuleViolationException e)
            {
                throw new InvalidApplicationServiceCallException(e.Message, e);
            }
        }

        public static async Task<T> Action<T>(Func<Task<T>> a)
        {
            Require.ArgNotNull(a, "action");
            try
            {
                return await a();
            }
            catch (DomainRuleViolationException e)
            {
                throw new InvalidApplicationServiceCallException(e.Message, e);
            }
        }
    }
}
