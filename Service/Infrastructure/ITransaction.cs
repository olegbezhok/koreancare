﻿using System;
using System.Threading.Tasks;

namespace Service.Infrastructure
{
    public interface ITransaction : IDisposable
    {
        Task Commit();
    }
}
