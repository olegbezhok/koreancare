﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service.Infrastructure
{
    public interface IUnitOfWork
    {
        ITransaction BeginTransaction(IsolationLevel isolotionLevel = IsolationLevel.ReadUncommitted);
    }
}
