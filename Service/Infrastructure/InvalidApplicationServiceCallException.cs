﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text;

namespace Service.Infrastructure
{
    public class InvalidApplicationServiceCallException : Exception
    {
        public InvalidApplicationServiceCallException()
        {
        }

        protected InvalidApplicationServiceCallException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public InvalidApplicationServiceCallException(string message)
            : base(message)
        {
        }

        public InvalidApplicationServiceCallException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
