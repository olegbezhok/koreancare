﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Service.Infrastructure
{
    internal class BidirectionalDictionary<TFirst, TSecond> : IEnumerable<KeyValuePair<TFirst, TSecond>>
    {
        private readonly IList<KeyValuePair<TFirst, TSecond>> mappingCollection =
            new List<KeyValuePair<TFirst, TSecond>>();

        public TSecond this[TFirst key] => mappingCollection.First(x => x.Key.Equals(key)).Value;

        public TFirst this[TSecond key] => mappingCollection.First(x => x.Value.Equals(key)).Key;

        internal void Add(TFirst first, TSecond second)
        {
            if (mappingCollection.Any(x => x.Key.Equals(first) || x.Value.Equals(second)))
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                    $"Duplicated key value pair {first} - {second}"));
            }

            mappingCollection.Add(new KeyValuePair<TFirst, TSecond>(first, second));
        }

        public IEnumerator<KeyValuePair<TFirst, TSecond>> GetEnumerator()
        {
            return mappingCollection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
