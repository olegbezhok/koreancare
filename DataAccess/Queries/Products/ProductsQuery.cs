﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DataAccess.Infrastructure;
using Domain.Queries.Products;

namespace DataAccess.Queries.Products
{
    public class ProductsQuery : IProductsQuery
    {
        private const string ListQuery = @"
IF OBJECT_ID('tempdb..#temp_products') IS NOT NULL
DROP TABLE #temp_products


SELECT ROW_NUMBER() OVER (ORDER BY p.Name) AS ROW_NUMBER(), p.ProductId , p.Name , p.ProductTypeId , p.Price , p.Description, c.CampaignId
INTO #temp_products
FROM Product p
JOIN Company c ON Product.CompanyId = C.CompanyId
JOIN ProductType pt ON pt.ProductTypeId = Product.ProductTypeId
WHERE p.Name LIKE @productName
AND (@companyId IS NULL OR c.CompanyId = @companyId)
AND (@productTypeId IS NULL OR p.ProductTypeId = @productTypeId)
AND (@productId IS NULL OR p.ProductId = @productId)

SELECT tp.ProductId, t.Name, t.ProductTypeId, t.Price, t.Description, t.CampaignId
FROM #temp_products tp
WHERE tp.ROW_NUMBER >= @offset AND tp.ROW_NUMBER <= @offset + @limit

SELECT COUNT(*) FROM #temp_products
";

        private readonly IDbContext _context;

        public ProductsQuery(IDbContext context)
        {
            _context = context;
        }

        public async Task<(IEnumerable<ProductReadModel> Products, int TotalCount)> Execute(int limit, int offset, int? productId = null)
        {
            var @params = new
            {
                limit,
                offset,
                productId
            };

            using (var reader = await _context.Connection.QueryMultipleAsync(
                ListQuery, @params, _context.Transaction))
            {
                var products = reader.Read<dynamic>().Select(p =>
                    new ProductReadModel
                    {
                        Name = p.Name,
                        Description = p.Description,
                        ProductId = p.ProductId
                    });
                var totalCount = reader.ReadFirst<int>();

                return (products, totalCount);
            }
        }
    }
}
