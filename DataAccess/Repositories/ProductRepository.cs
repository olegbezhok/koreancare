﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Infrastructure;
using Domain.Entities;
using Domain.Repositories;

namespace DataAccess.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IDbContext _dbContext;

        public ProductRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Product> Get(int productId)
        {
            return await _dbContext.Session.GetAsync<Product>(productId);
        }

        public async Task Save(Product product)
        {
            await _dbContext.Session.SaveOrUpdateAsync(product);
        }
    }
}
