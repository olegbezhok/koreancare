﻿using Domain.Entities;
using FluentNHibernate;
using FluentNHibernate.Mapping;

namespace DataAccess.Mapping
{
    internal class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Table("[dbo].[Product]");
            Where("active = 1");

            Id(Reveal.Member<Product>("productId")).Column("ProductId").GeneratedBy.Identity();

            Map(p => p.Price, "Price");
            Map(p => p.ProductType, "ProductTypeId").CustomType<ProductTypeMap>();
            Map(p => p.Company, "CompanyId").CustomType<CompanyMap>();
        }
    }
}
