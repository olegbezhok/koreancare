﻿using System;
using System.Data.Common;
using System.Linq;
using DataAccess.Infrastructure;
using Domain.Entities.ValueTypes;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;

namespace DataAccess.Mapping
{
    public class CompanyMap : ImmutableUserType<Company>
    {
        public override object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            var dbValue = (int)NHibernateUtil.Int32.NullSafeGet(rs, names[0], session);

            return Mapping[dbValue];
        }

        public override void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            var domainValue = (Company)value;
            NHibernateUtil.Int32.NullSafeSet(cmd, Mapping[domainValue], index, session);
        }

        public static bool Exists(int type)
        {
            return Mapping.Any(k => k.Key == type);
        }

        public static Company ToDomain(int type)
        {
            return Mapping[type];
        }

        public static int ToDbValue(Company domain)
        {
            return Mapping[domain];
        }

        public override SqlType[] SqlTypes => new[] { SqlTypeFactory.Int32 };

        private static readonly BidirectionalDictionary<int, Company> Mapping =
            new BidirectionalDictionary<int, Company>
            {
            };
    }
}
