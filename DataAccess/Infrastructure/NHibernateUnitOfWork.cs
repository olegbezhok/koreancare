﻿using System.Data;
using NHibernate;
using Service.Infrastructure;
using ITransaction = Service.Infrastructure.ITransaction;

namespace DataAccess.Infrastructure
{
    public class NHibernateUnitOfWork : IUnitOfWork, IDbContext
    {
        private ISession _currentSession;
        private readonly ISessionFactoryProvider _sessionFactoryProvider;

        public NHibernateUnitOfWork(ISessionFactoryProvider sessionFactoryProvider)
        {
            _sessionFactoryProvider = sessionFactoryProvider;
        }

        public ITransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadUncommitted)
        {
            if (_currentSession == null || !_currentSession.IsOpen)
            {
                _currentSession = _sessionFactoryProvider.Build()
                    .WithOptions()
                    .FlushMode(FlushMode.Always)
                    .OpenSession();
            }

            return new NHibernateTransaction(_currentSession, isolationLevel);
        }

        public ISession Session => _currentSession;
        public IDbConnection Connection => _currentSession.Connection;

        public IDbTransaction Transaction
        {
            get
            {
                using (var command = _currentSession.Connection.CreateCommand())
                {
#pragma warning disable 618
                    _currentSession.Transaction.Enlist(command);
#pragma warning restore 618
                    return command.Transaction;
                }
            }
        }
    }
}
