﻿using System;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace DataAccess.Infrastructure
{
    public abstract class ImmutableUserType<T> : IUserType
    {
        public abstract object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner);
        public abstract void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session);
        public abstract SqlType[] SqlTypes { get; }

        [ExcludeFromCodeCoverage]
        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        [ExcludeFromCodeCoverage]
        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        [ExcludeFromCodeCoverage]
        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        [ExcludeFromCodeCoverage]
        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public Type ReturnedType
        {
            get { return typeof(T); }
        }

        public bool IsMutable
        {
            get { return false; }
        }
    }
}
