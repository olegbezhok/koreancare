﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataAccess.Infrastructure
{
    public interface IDbContext
    {
        ISession Session { get; }
        IDbConnection Connection { get; }
        IDbTransaction Transaction { get; }
    }
}
