﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace DataAccess.Infrastructure
{
    public interface ISessionFactoryProvider
    {
        ISessionFactory Build();
    }
    public class SessionFactoryProvider : ISessionFactoryProvider
    {
        private readonly string _connectionString;
        private static ISessionFactory _instance;
        private static readonly object syncObj = new object();

        public SessionFactoryProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ISessionFactory Build()
        {
            if (_instance == null)
            {
                lock (syncObj)
                {
                    if (_instance == null)
                    {
                        _instance = Fluently.Configure()
                            .Database(MsSqlConfiguration.MsSql2012.ConnectionString(_connectionString))
                            .Mappings(x => x.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                            .BuildSessionFactory();
                    }
                }
            }

            return _instance;
        }
    }
}
