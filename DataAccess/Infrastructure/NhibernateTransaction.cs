﻿using System;
using System.Data;
using System.Threading.Tasks;
using NHibernate;

namespace DataAccess.Infrastructure
{
    public class NHibernateTransaction : Service.Infrastructure.ITransaction
    {
        private readonly ITransaction _transaction;
        private readonly ISession _session;

        public NHibernateTransaction(
            ISession session,
            IsolationLevel isolationLevel = IsolationLevel.ReadUncommitted)
        {
            _session = session;

            _transaction = session.BeginTransaction(isolationLevel);
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManaged)
        {
            if (disposeManaged)
            {
                _transaction.Dispose();
                _session.Dispose();
            }
        }
    }
}
